package com.geekfox.hotswap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootLab1HotswapApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringbootLab1HotswapApplication.class, args);
    }

}
